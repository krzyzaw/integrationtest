﻿using Xunit;
using Xunit.Abstractions;

namespace IntegrationTest
{
    public class ImdbMovieDetails : TestBase
    {
        public ImdbMovieDetails(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void return_json_response()
        {
            var json = GetDataById("tt0330373");

            _output.WriteLine(json);

            var ex = Record.Exception(() => DeserializeJson(json));

            Assert.Null(ex);
        }

        [Fact]
        public void returns_details__aobut_Harry_Potter_and_the_Goblet_of_Fire_when_given_its_id()
        {
            var json = GetDataById("tt0330373");

            dynamic harryPotterAndTheGlobalOfFireDetails = DeserializeJson(json);

            Assert.Equal("Harry Potter and the Goblet of Fire", harryPotterAndTheGlobalOfFireDetails["Title"]);
            Assert.Equal("2005", harryPotterAndTheGlobalOfFireDetails["Year"]);
            Assert.Equal("tt0330373", harryPotterAndTheGlobalOfFireDetails["imdbID"]);
            Assert.Equal("Mike Newell", harryPotterAndTheGlobalOfFireDetails["Director"]);
            Assert.Equal("movie", harryPotterAndTheGlobalOfFireDetails["Type"]);
            Assert.Equal("UK, USA", harryPotterAndTheGlobalOfFireDetails["Country"]);
            Assert.Equal("English, French", harryPotterAndTheGlobalOfFireDetails["Language"]);
            Assert.NotNull(harryPotterAndTheGlobalOfFireDetails["imdbRating"]);
        }
    }
}