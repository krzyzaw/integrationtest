﻿namespace IntegrationTest
{
    public class MovieSearchResult
    {
        public string Title { get; set; }
        public string Id { get; set; }
    }
}