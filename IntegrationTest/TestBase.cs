﻿using System.Net.Http;
using System.Web.Script.Serialization;
using Xunit.Abstractions;

namespace IntegrationTest
{
    public abstract class TestBase
    {
        protected readonly ITestOutputHelper _output;

        protected TestBase(ITestOutputHelper output)
        {
            _output = output;
        }

        public string GetDataByName(string name)
        {
            string url = "http://www.omdbapi.com?s=" + name;
            return new HttpClient().GetAsync(url).Result.Content.ReadAsStringAsync().Result;
        }

        public string GetDataById(string id)
        {
            string url = "http://www.omdbapi.com?i=" + id;
            return new HttpClient().GetAsync(url).Result.Content.ReadAsStringAsync().Result;
        }

        public static dynamic DeserializeJson(string result)
        {
            return new JavaScriptSerializer().Deserialize<dynamic>(result);
        }
    }
}
