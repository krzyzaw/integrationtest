﻿using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace IntegrationTest
{
    public class ImdbSearchApi : TestBase
    {
        public ImdbSearchApi(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void returns_response()
        {
            var results = GetDataByName("Harry Potter");

            _output.WriteLine(results);

            Assert.NotEmpty(results);
        }

        [Fact]
        public void returns_json_response()
        {
            var result = GetDataByName("Harry Potter");

            var ex = Record.Exception(() => DeserializeJson(result)); 

            Assert.Null(ex);
        }

        [Fact]
        public void returns_Harry_Potter_and_the_Goblet_of_Fire_among_other_result_when_searching_for_Harry_Potter()
        {
            var json = GetDataByName("Harry Potter");

            dynamic deserializeJson = DeserializeJson(json);

            dynamic[] searchResult = deserializeJson["Search"];

            Assert.True(searchResult.Length > 1);

            dynamic harryPotterAndTheGlobalOfFire = searchResult.SingleOrDefault(x => x["Title"] == "Harry Potter and the Goblet of Fire");

            Assert.NotNull(harryPotterAndTheGlobalOfFire);

            Assert.Equal("Harry Potter and the Goblet of Fire", harryPotterAndTheGlobalOfFire["Title"]);
            Assert.Equal("2005", harryPotterAndTheGlobalOfFire["Year"]);
            Assert.Equal("tt0330373", harryPotterAndTheGlobalOfFire["imdbID"]);
        }
    }
}