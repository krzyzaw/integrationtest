﻿namespace IntegrationTest
{
    public class Movie
    {
        public string Title { get; set; }
        public string Directory { get; set; }
        public double ImdbRating { get; set; }
        public int Year { get; set; }
    }
}